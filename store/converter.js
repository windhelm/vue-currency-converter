import axios from 'axios'
import * as types from './mutation-types'

function isEmptyObject(obj) {
    for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
            return false;
        }
    }
    return true;
}

export const state = () => ({
    result: [],
    error: null,
    success: null,
    currencies: ['USD','JPY','BGN','CZK','DKK','GBP','HUF','PLN','RON',
        'SEK','CHF','ISK','NOK','HRK','RUB','TRY','AUD','BRL','CAD','CNY','HKD',
        'IDR','ILS','INR','KRW','MXN','MYR','NZD','PHP','SGD','THB','ZAR'
    ],
    base: 'USD',
    selected: [],
    date_start: new Date().toISOString().substr(0, 10),
    date_finish: new Date().toISOString().substr(0, 10)
})

export const getters = {
    result: state => state.result,
    success: state => state.success,
    error: state => state.error,
    currencies: state => state.currencies
}

export const mutations = {

    [types.CLEAR_MESSAGE] (state, data ) {

        state.result = null;
        state.error = null;
        state.success = null;
    },

    [types.CONVERT_SUCCESS] (state, data ) {
        let buf = [];
        for(let key in data.rates){
            buf.push({date: key, currency: data.rates[key]});
        }
        state.result = buf;
        state.success = true;
    },

    [types.CONVERT_FAILURE] (state, error) {
        state.result = null;
        state.success = false;
        state.error = error;
    },

    [types.UPDATE_SELECTED] (state, data ) {
        state.selected = data;
    },

    [types.UPDATE_BASE] (state, data ) {
        state.base = data;
    },

    [types.UPDATE_DATE_START] (state, data ) {
        state.date_start = data;
    },

    [types.UPDATE_DATE_FINISH] (state, data ) {
        state.date_finish = data;
    },
}

export const actions = {

    async convert({ commit }, params){
        try {


            const { data } = await axios.get(`https://api.exchangeratesapi.io/history`, {
                params: params
            });

            if (isEmptyObject(data.rates)){
                commit(types.CONVERT_FAILURE, 'Данных за такой промежуток не найдено')

            }else{
                commit(types.CONVERT_SUCCESS, data );
            }

        } catch (e) {
            console.log(e);
            commit(types.CONVERT_FAILURE, e.response.data.error)
        }
    }

}
