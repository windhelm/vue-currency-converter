export default function ({ route,app,redirect }) {

    // Проверяем есть ли в пути язык, если нет, то редиректим на /defaultLocale + path

    // Получаем список доступных локалей
    let locales = app.i18n.locales;


    for (let i=0; i < locales.length; i++){
        // Есть язык делаем return из мидлвара
        if (route.path.toLowerCase().indexOf(locales[i].code.toLowerCase()) !== -1){
            return;
        }
    }

    // Языки не найдены редиректим
    redirect("/" + app.i18n.defaultLocale + route.path);

}
