export default {
    welcome: 'Добро пожаловать',
    currency: 'Валюта',
    currencies: 'Валюты',
    base :'Базовая валюта',
    convert: 'Конвертировать',
    date_start: 'Дата начала',
    date: 'Дата',
    date_finish: 'Дата конца',
    go_convert: 'Перейти к конвертированию',
    success: 'Данные успешно обновлены',
    error: 'Ошибка',
    res: 'Результат',
    res_not_found: 'Результатов не найдено'
}
