export default {
    welcome: 'Welcome',
    currency: 'Currency',
    currencies: 'Currencies',
    base :'Base currency',
    convert: 'Convert',
    date_start: 'Date start',
    date: 'Date',
    date_finish: 'Date finish',
    go_convert: 'Go to convert',
    success: 'Data update successfully',
    error: 'error',
    res: 'Result',
    res_not_found: 'Result not found'
}
