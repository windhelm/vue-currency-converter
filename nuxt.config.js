module.exports = {
  /*
  ** Headers of the page
  */
    router: {
        middleware: 'language'
    },
  plugins:[
      '~plugins/vue-notifications','~plugins/vuetify'
  ],
  modules: [
      ['nuxt-i18n', {
          defaultLocale: 'en',
          strategy: 'prefix',
          loadLanguagesAsync: true,
          lazy: true,
          seo: false,
          locales: [
              {
                  code: 'en',
                  file: 'en-US.js',
                  iso :'en-US',
                  name: 'English'
              },
              {
                  code: 'ru',
                  file: 'ru-RU.js',
                  iso: 'ru-RU',
                  name: 'Русский'
              }
          ],
          langDir: 'lang/',
      }]
  ],
  css: ['vuetify/dist/vuetify.min.css','material-design-icons-iconfont/dist/material-design-icons.css','~/assets/scss/main.scss'],
  head: {
    title: 'vue-currency-converter',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['axios','xml2js'],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
}

